NodeJS Passport Boilerplate
============

**THIS REPO HAS ALL DEPENDENCIES INCLUDED**

##[Demo](http://passport.diki.io)

User system in nodejs illustrating the use of **passport** in **express**, **jade** and **mongoose** environment
together with front pages for login, signup and profile mock built with Twitter Bootstrap

Includes login with Facebook, Twitter, Github and Google together with Passport LocalStrategy

Many thanks to https://github.com/madhums/node-express-mongoose-demo since this project is stripped and modified version

Technology
------------

| On The Server | On The Client  |
| ------------- | -------------- |
| Express       | Bootstrap 2    |
| EJS           | Backbone.js    |
| Passport      | jQuery         |
| Mongoose      | Underscore.js  |

Prerequisite
-------------
MongoDB access. if you have mongodb installed locally and running on default port no need to change anything. 

Installation
-------------

1. git clone https://github.com/diki/nodejs-passport-boilerplate.git
2. cd nodejs-passport-boilerplate
3. npm install (No need to run this as this repo has all the dependancies included)
4. modify config.js file under config folder (change mongodb connection string. if you have mongodb installed locally and running on default port no need to change anything. )
5. npm start

License
------------

MIT